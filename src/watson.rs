use std::cmp::Ordering;
use std::convert::TryInto;
use std::error::Error;
use std::fmt;
use std::fs;
use std::path::Path;

use serde::Deserialize;
use serde_json;
use serde_json::Value;
use time::{now, Duration};

use crate::utils::homedir;

// the default time between tasks to still consider it to belong to the current "workday"
static MAX_SECONDS_BETWEEN_TASKS: i64 = 60 * 5;

pub fn watson_task(
    project: &Option<String>,
    tag: &Option<String>,
) -> Result<Option<Task>, Box<dyn Error>> {
    let active_task = Task::from_watson_state_file()?;
    if Task::matches(&active_task, project, tag) {
        Ok(active_task)
    } else {
        Ok(None)
    }
}

pub fn watson_time(max_minutes_between_tasks: Option<i64>) -> Result<i64, Box<dyn Error>> {
    let max_seconds_between_tasks = match max_minutes_between_tasks {
        Some(t) => t * 60,
        None => MAX_SECONDS_BETWEEN_TASKS,
    };
    let current_task: Option<Task> = Task::from_watson_state_file()?;
    let finished_tasks: Vec<Task> = Task::from_watson_frames_file()?;
    let mut total_time: i64 = match current_task {
        Some(current_task) => {
            let current_duration = current_task.duration()?.num_seconds();
            if let Some(last_finished_task) = finished_tasks.first() {
                if current_task.distance(last_finished_task)?
                    > Duration::seconds(max_seconds_between_tasks)
                {
                    return Ok(current_duration); // if the last finished task is long ago, we're done here
                }
            }
            current_duration
        }
        None => return Ok(0), // if there's no task active, we're done here
    };
    let mut previous_task: Option<Task> = None;
    for task in finished_tasks {
        match previous_task {
            Some(previous_task) => {
                let distance = task.distance(&previous_task)?;
                if distance < Duration::seconds(max_seconds_between_tasks) {
                    total_time += task.duration()?.num_seconds();
                } else {
                    break;
                }
            }
            None => {
                total_time += task.duration()?.num_seconds();
            }
        }
        previous_task = Some(task);
    }
    Ok(total_time)
}

#[derive(Deserialize, Debug)]
pub struct Task {
    project: String,
    tags: Vec<String>,
    start: Option<i64>,
    end: Option<i64>,
}
impl Task {
    #[cfg(test)]
    pub fn new(project: String, tags: Vec<String>, start: Option<i64>, end: Option<i64>) -> Self {
        Self {
            project,
            tags,
            start,
            end,
        }
    }

    pub fn from_watson_state_file() -> Result<Option<Self>, Box<dyn Error>> {
        let home = homedir()?;
        let state_file = if cfg!(target_os = "linux") {
            Path::new(&home)
                .join(".config")
                .join("watson")
                .join("state")
        } else {
            Path::new(&home)
                .join("Library")
                .join("Application Support")
                .join("watson")
                .join("state")
        };
        let file_content: String = fs::read_to_string(state_file)?;
        if file_content == "{}" {
            return Ok(None);
        }
        match serde_json::from_str(&file_content) {
            Ok(task) => Ok(Some(task)),
            Err(e) => Err(Box::new(e)),
        }
    }

    pub fn from_watson_frames_file() -> Result<Vec<Self>, Box<dyn Error>> {
        let home = homedir()?;
        let frames_file = if cfg!(target_os = "linux") {
            Path::new(&home)
                .join(".config")
                .join("watson")
                .join("frames")
        } else {
            Path::new(&home)
                .join("Library")
                .join("Application Support")
                .join("watson")
                .join("frames")
        };
        let file_content: String = fs::read_to_string(frames_file)?;
        let frames: Vec<Vec<Value>> = match serde_json::from_str(&file_content) {
            Ok(frames) => frames,
            Err(e) => return Err(Box::new(e)),
        };
        let tasks: Vec<Self> = frames
            .into_iter()
            .filter_map(|frame| Task::from_frame(frame))
            .rev()
            .collect();
        Ok(tasks)
    }

    pub fn matches(task: &Option<Task>, project: &Option<String>, tag: &Option<String>) -> bool {
        let matches_project: bool = match task {
            Some(task) => task.matches_project(project),
            None => false,
        };
        let matches_tag: bool = match task {
            Some(task) => task.matches_tag(tag),
            None => false,
        };

        matches_project || matches_tag
    }

    fn from_frame(frame: Vec<Value>) -> Option<Self> {
        //! This generates a task from the lists present in watson's frames file,
        //! assuming the frames file is properly formatted as expected by watson.
        let start = frame[0].as_i64();
        let end = frame[1].as_i64();
        let project = match frame[2].as_str() {
            Some(project) => String::from(project),
            None => return None,
        };
        let tags: Vec<String> = match frame[4].as_array() {
            Some(tags) => tags
                .iter()
                .filter_map(|tag| tag.as_str())
                .map(|tag| String::from(tag))
                .collect(),
            None => return None,
        };
        Some(Self {
            start,
            end,
            project,
            tags,
        })
    }

    pub fn distance(&self, other: &Self) -> Result<Duration, Box<dyn Error>> {
        if vec![self.start, other.start].iter().any(|t| t.is_none()) {
            return Err(Box::new(WatsonError::MalformedFrame));
        }

        if self < other {
            Ok(Duration::seconds(
                (other.start.unwrap() - self.end.unwrap()).try_into()?,
            ))
        } else {
            Ok(Duration::seconds(
                (self.start.unwrap() - other.end.unwrap()).try_into()?,
            ))
        }
    }

    pub fn duration(&self) -> Result<Duration, Box<dyn Error>> {
        match self.start {
            Some(start) => match self.end {
                Some(end) => {
                    let duration = Duration::seconds((end - start).try_into()?);
                    Ok(duration)
                }
                None => {
                    let current_time: i64 = now().to_timespec().sec;
                    let duration = Duration::seconds(current_time - start);
                    Ok(duration)
                }
            },
            None => Err(Box::new(WatsonError::MalformedFrame)),
        }
    }

    fn matches_project(&self, project: &Option<String>) -> bool {
        match project {
            Some(p) => self.project == *p,
            None => true,
        }
    }

    fn matches_tag(&self, tag: &Option<String>) -> bool {
        match tag {
            Some(t) => self.tags.contains(t),
            None => true,
        }
    }
}
impl fmt::Display for Task {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.tags.is_empty() {
            write!(f, "{}", self.project)
        } else {
            write!(f, "{} {:?}", self.project, self.tags)
        }
    }
}
impl PartialEq for Task {
    fn eq(&self, other: &Self) -> bool {
        self.start == other.start && self.end == other.end
    }
}
impl PartialOrd for Task {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        //! For now this assumes we're only handling completed tasks
        //! from watson's frames file here.
        if vec![self.start, self.end, other.start, other.end]
            .iter()
            .any(|t| t.is_none())
        {
            return None;
        }
        // from here on we know nothing is None and can use Option.unwrap()

        if self.end < other.start {
            Some(Ordering::Less)
        } else {
            Some(Ordering::Greater)
        }
    }
}

#[derive(Debug)]
enum WatsonError {
    MalformedFrame,
}
impl fmt::Display for WatsonError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            WatsonError::MalformedFrame => "Frame malformed. Check your watson's frames file",
        };
        write!(f, "{}", msg)
    }
}
impl Error for WatsonError {
    fn description(&self) -> &str {
        match self {
            WatsonError::MalformedFrame => "Frame malformed. Check your watson's frames file",
        }
    }
}

#[cfg(test)]
mod test_task_matches {
    use super::*;

    #[test]
    fn should_return_true_for_any_task_being_active() {
        let active_task = Some(Task::new(String::from("foo"), vec![], None, None));
        let project = None;
        let tag = None;

        let result = Task::matches(&active_task, &project, &tag);
        assert!(result)
    }

    #[test]
    fn should_return_false_for_any_task_being_inactive() {
        let active_task = None;
        let project = None;
        let tag = None;

        let result = Task::matches(&active_task, &project, &tag);
        assert!(!result)
    }

    #[test]
    fn should_return_true_for_project_being_active() {
        let active_task = Some(Task::new(String::from("foo"), vec![], None, None));
        let project = Some(String::from("foo"));
        let tag = None;

        let result = Task::matches(&active_task, &project, &tag);
        assert!(result)
    }

    #[test]
    fn should_return_true_for_tag_being_active() {
        let active_task = Some(Task::new(
            String::from("foo"),
            vec![String::from("bar")],
            None,
            None,
        ));
        let project = None;
        let tag = Some(String::from("bar"));

        let result = Task::matches(&active_task, &project, &tag);
        assert!(result)
    }

    #[test]
    fn should_return_false_for_project_being_inactive() {
        let active_task = None;
        let project = Some(String::from("foo"));
        let tag = None;

        let result = Task::matches(&active_task, &project, &tag);
        assert!(!result)
    }

    #[test]
    fn should_return_false_for_tag_being_inactive() {
        let active_task = None;
        let project = None;
        let tag = Some(String::from("bar"));

        let result = Task::matches(&active_task, &project, &tag);
        assert!(!result)
    }
}

#[cfg(test)]
mod test_task_duration {
    use super::*;

    #[test]
    fn should_return_ok_with_correct_duration_for_start_and_end_present() {
        let task = Task::new(
            String::from("foo"),
            Vec::new(),
            Some(1588181741),
            Some(1588181751),
        );
        let expected = Duration::seconds(10);
        let result = task.duration().unwrap();
        assert_eq!(result, expected)
    }

    #[test]
    fn should_return_error_for_start_missing() {
        let task = Task::new(String::from("foo"), Vec::new(), None, Some(1588181751));
        assert!(task.duration().is_err())
    }

    #[test]
    fn should_return_ok_with_duration_until_now_for_end_missing() {
        let current_time = now().to_timespec().sec;
        let duration = 400;
        let task = Task::new(
            String::from("foo"),
            Vec::new(),
            Some((current_time - duration).try_into().unwrap()),
            None,
        );
        let expected = Duration::seconds(duration);
        let result = task.duration().unwrap();
        assert_eq!(result, expected)
    }
}

#[cfg(test)]
mod test_task_distance {
    use super::*;

    #[test]
    fn should_return_correct_distance_to_future_task() {
        let start_first = 1573336531;
        let end_first = start_first + 900;
        let diff_between_tasks = 12;
        let start_second = end_first + diff_between_tasks;
        let end_second = start_second + 540;
        let first = Task::new(
            String::from("foo"),
            Vec::new(),
            Some(start_first),
            Some(end_first),
        );
        let second = Task::new(
            String::from("bar"),
            Vec::new(),
            Some(start_second),
            Some(end_second),
        );

        let result = first.distance(&second).unwrap();
        assert_eq!(
            result,
            Duration::seconds(diff_between_tasks.try_into().unwrap())
        );
    }

    #[test]
    fn should_return_correct_distance_to_past_task() {
        let start_first = 1573336531;
        let end_first = start_first + 900;
        let diff_between_tasks = 12;
        let start_second = end_first + diff_between_tasks;
        let end_second = start_second + 540;
        let first = Task::new(
            String::from("foo"),
            Vec::new(),
            Some(start_first),
            Some(end_first),
        );
        let second = Task::new(
            String::from("bar"),
            Vec::new(),
            Some(start_second),
            Some(end_second),
        );

        let result = second.distance(&first).unwrap();
        assert_eq!(
            result,
            Duration::seconds(diff_between_tasks.try_into().unwrap())
        );
    }

    #[test]
    fn should_return_correct_distance_between_active_and_finished_task() {
        let start_finished = 1573336531;
        let end_finished = start_finished + 900;
        let diff_between_tasks = 42;
        let start_active = end_finished + diff_between_tasks;
        let finished = Task::new(
            String::from("foo"),
            Vec::new(),
            Some(start_finished),
            Some(end_finished),
        );
        let active = Task::new(String::from("bar"), Vec::new(), Some(start_active), None);

        let result = active.distance(&finished).unwrap();
        assert_eq!(
            result,
            Duration::seconds(diff_between_tasks.try_into().unwrap())
        );
    }

    #[test]
    fn should_return_error_for_start_times_missing() {
        let start_missing = Task::new(String::from("foo"), Vec::new(), None, Some(1588181751));
        let nothing_missing = Task::new(
            String::from("foo"),
            Vec::new(),
            Some(1588181751),
            Some(1588181771),
        );
        let both_missing = Task::new(String::from("foo"), Vec::new(), None, None);

        assert!(start_missing.distance(&both_missing).is_err());
        assert!(start_missing.distance(&nothing_missing).is_err());
        assert!(nothing_missing.distance(&start_missing).is_err());
        assert!(nothing_missing.distance(&both_missing).is_err());
    }
}
