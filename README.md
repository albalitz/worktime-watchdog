# Worktime Watchdog
This is a small helper to keep the time at work in a reasonable time to avoid working too much.

> **Note:** this is in early development and will very likely change.

## Config
You can specify the config file to use via `--config <path to your config>`.
When you don't specify one, the default will be used: $HOME/.config/worktime-watchdog/config.yml

An example config may look like this:
```yaml
# list of conditions to check to decide whether or not to alert about working too much
conditions:
  # consider your system's uptime (in hours)
  - name: uptime
    max_uptime: 9h # defaults to 8h if omitted
    max_minutes_between_sessions: 15 # defaults to 10 minutes if omitted
  # consider the duration of a set of watson tasks your workday
  - name: watson_time
    max_time: 8h
    max_minutes_between_tasks: 1  # defaults to 5 if omitted
  # check your network interfaces if you have an IP in this range
  - name: ip_in_range
    value: 10.11.0.0/24
  # check if your ip address ends with a specific string
  - name: ip_ends_with
    value: .11
  # check if your ip address starts with a specific string
  - name: ip_starts_with
    value: 10.11.0.1
  # check if a specific network device exists
  - name: tunnel_device
    value: tun0
  # check if a watson task is active by its project
  - name: watson_task
    project: foo
  # check that a watson task with a given is active by its tag
  - name: watson_task
    tag: foo

# omitting this completely shows a default notification telling you about matched conditions
notifications:
  title: GO HOME
  body: You worked enough for today.
```

### Notes
The conditions are checked in the order in which you list them in your config.  
If one condition fails, following conditions are not checked.

_All_ of the conditions have to match for your work time to be considered enough.

#### Available Features
- `watson`  
  Enables checking for watson-related conditions. Enabled by default.
- `uptime`  
  Enables checking for system uptime. Enabled by default.
- `network`  
  Enables checking for network-related conditions.
- `notifications`  
  Enables sending a notification when conditions matched.

See https://doc.rust-lang.org/cargo/reference/features.html#command-line-feature-options for instructions on how to customize features.

#### Uptime and Watson Time
The uptime and watson_time conditions accept the max time to be configured using the following unit suffixes:
- s (seconds)
- m (minutes)
- h (hours)

When you don't specify any suffix, it is interpreted as hours.

##### Uptime
The uptime is calculated by trying to read sessions from `/var/log/wtmp` - that way the `max_minutes_between_sessions` attribute of the uptime condition can be used to account for reboots and short downtimes like shutting down and then walking to a meeting or something like that.
When you don't want to account for reboots, set `max_minutes_between_sessions` explicitly to 0.

If that fails, `/proc/uptime` will be read, which may not work on all systems either.

##### Watson Task/Time
See https://github.com/TailorDev/Watson

For now this only supports checking for an _active_ task.  
A check for e.g. a specific tag being _inactive_ is currently not supported.
But merge requests are welcome.

## Usage
This is not intended to run as a daemon - to allow more flexibility in its usage, allowing you to choose whether to run it via cron, or e.g. in your Polybar to display your working time there, etc.

### Exit Codes
When all of your configured conditions are met, the tool exits with `0`.  
If any condition is not met, it exits with `1`.

### Use as cronjob
```
* * * * * export XDG_RUNTIME_DIR=/run/user/$(id -u); /path/to/worktime-watchdog && notify-send "GO HOME"
```
Note: Depending on your system, you may need to `export DISPLAY=:0` or something similar instead.

### Use as MacOS Agent
Put a file like this in `~/Library/LaunchAgents/local.worktime-watchdog.plist`, then load it using `launchctl load ~/Library/LaunchAgents/local.worktime-watchdog.plist`:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
    <dict>
        <key>Label</key>
        <string>local.worktime-watchdog</string>
        <key>Program</key>
        <string>/path/to/worktime-watchdog</string>
        <key>StartInterval</key>
        <integer>60</integer>
    </dict>
</plist>
```

Note: You'll want to enable the `notifications` feature when compiling/installing `worktime-watchdog`.

### Use as Polybar module
Configure the module anywhere in your polybar config file:
```
[module/worktime]
type = custom/script
format = "<label>"
label = %output%
exec = if $HOME/bin/worktime-watchdog &> /dev/null; then echo "!! GO HOME !!"; else echo ""; fi
tail = false
interval = 60
```
Then place it in `modules-left`, `modules-center`, or `modules-right` as "worktime".
