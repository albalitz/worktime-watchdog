mod conditions;
mod config;
mod utils;

#[cfg(feature = "network")]
mod ip;
#[cfg(feature = "network")]
mod network_device;

#[cfg(feature = "uptime")]
mod uptime;

#[cfg(feature = "watson")]
mod watson;

use std::process::exit;

use clap::{crate_name, crate_version};
use clap::{App, Arg};
#[cfg(feature = "notifications")]
use notify_rust::{Notification, Timeout as NotificationTimeout};

use config::Config;
use utils::homedir;

#[cfg(feature = "notifications")]
fn notify(config: &Config) -> Result<(), notify_rust::error::Error> {
    let notifications_config = config.notifications();
    let title = notifications_config.title();
    let body = notifications_config.body();
    Notification::new()
        .summary(title)
        .body(body)
        .timeout(NotificationTimeout::Never)
        .show()?;
    Ok(())
}

fn main() {
    let home = match homedir() {
        Ok(h) => h,
        Err(e) => {
            eprintln!("{}", e);
            exit(1);
        }
    };
    let default_config_filename: String = format!("{}/.config/worktime-watchdog/config.yml", home);
    let args = App::new(crate_name!())
        .version(crate_version!())
        .about("A tool for keeping your working time in check.")
        .arg(
            Arg::with_name("config")
                .help("Path to the config file you wish to use")
                .long("--config")
                .takes_value(true)
                .default_value(&default_config_filename),
        )
        .get_matches();
    let config_filename = args.value_of("config").unwrap();
    let config = match Config::from_file(config_filename) {
        Ok(cfg) => cfg,
        Err(e) => {
            eprintln!("Error loading config from '{}': {}", config_filename, e);
            exit(-2);
        }
    };

    if config.conditions().iter().all(|c| c.matches()) {
        #[cfg(feature = "notifications")]
        if let Err(e) = notify(&config) {
            eprintln!("Failed to notify: {e}");
        }
        exit(0);
    } else {
        eprintln!("Not all conditions matched.");
        exit(1);
    }
}
