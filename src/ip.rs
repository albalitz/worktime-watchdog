use std::net::IpAddr;

use pnet::datalink;

pub fn ip_in_range(range: String) -> Option<String> {
    //! Check if any interface has an IP in the given range and return the first match, if there is
    //! any. If there is no match, return None.
    let ip_range: Vec<&str> = range.split('/').collect();
    let ip_address: IpAddr = match ip_range[0].parse() {
        Ok(ip) => ip,
        Err(e) => {
            eprintln!("Error parsing ip address: {}", e);
            return None;
        }
    };
    for interface in datalink::interfaces().iter() {
        for network in interface.ips.iter() {
            if network.contains(ip_address) {
                return Some(network.ip().to_string());
            }
        }
    }

    None
}

pub fn ip_starts_with(start: &String) -> Option<String> {
    for interface in datalink::interfaces().iter() {
        for network in interface.ips.iter() {
            match network.ip() {
                IpAddr::V4(net) => {
                    let net: String = net.to_string();
                    if net.starts_with(start) {
                        return Some(net);
                    }
                }
                _ => {}
            };
        }
    }

    None
}

pub fn ip_ends_with(start: &String) -> Option<String> {
    for interface in datalink::interfaces().iter() {
        for network in interface.ips.iter() {
            match network.ip() {
                IpAddr::V4(net) => {
                    let net: String = net.to_string();
                    if net.ends_with(start) {
                        return Some(net);
                    }
                }
                _ => {}
            };
        }
    }

    None
}

#[cfg(test)]
mod test_ip_in_range {
    use super::*;

    #[test]
    fn should_find_any_ip() {
        let range = "127.0.0.1/32".to_string();
        let matching_ip = ip_in_range(range);

        assert_eq!(matching_ip, Some("127.0.0.1".to_string()));
    }
}
