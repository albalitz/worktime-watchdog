use dirs::home_dir;

pub fn parse_time_to_seconds(mut time: String) -> Result<i64, String> {
    match time.parse::<i64>() {
        Ok(plain_number) => return Ok(plain_number * 60 * 60),
        Err(_) => {}
    };

    if time.chars().all(|c| c.is_alphabetic()) {
        return Err("Duration may not be an empty string!".to_string());
    }
    let suffix = time.split_off(time.len() - 1);
    let seconds: i64 = match time.parse::<i64>() {
        Ok(s) => s,
        Err(e) => return Err(e.to_string()),
    };
    let multiplicator = match suffix.as_str() {
        "s" => 1,
        "m" => 60,
        "h" => 60 * 60,
        _ => return Err(format!("Not a valid suffix: {}", suffix)),
    };

    Ok(seconds * multiplicator)
}

pub fn homedir() -> Result<String, String> {
    match home_dir() {
        Some(hd) => match hd.to_str() {
            Some(path) => Ok(path.to_string()),
            None => Err(String::from(
                "Your home directory contains non-UTF8 characters we can't work with",
            )),
        },
        None => Err(String::from("Can't find your home directory.")),
    }
}

#[cfg(test)]
mod test_parse_time_to_seconds {
    use super::*;

    #[test]
    fn should_parse_number_with_valid_suffix_to_seconds() {
        assert_eq!(parse_time_to_seconds(String::from("42s")).unwrap(), 42);
        assert_eq!(parse_time_to_seconds(String::from("480m")).unwrap(), 28800);
        assert_eq!(parse_time_to_seconds(String::from("8h")).unwrap(), 28800);
    }

    #[test]
    fn should_assume_hours_without_suffix() {
        assert_eq!(parse_time_to_seconds(String::from("8")).unwrap(), 28800);
    }

    #[test]
    fn should_return_error_for_unknown_suffix() {
        assert!(parse_time_to_seconds(String::from("42x")).is_err());
    }

    #[test]
    fn should_return_error_for_missing_numbers() {
        assert!(parse_time_to_seconds(String::from("foo")).is_err());
    }
}
