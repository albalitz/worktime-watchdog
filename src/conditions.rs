use serde::Deserialize;

#[cfg(feature = "network")]
use crate::ip::{ip_ends_with, ip_in_range, ip_starts_with};
#[cfg(feature = "network")]
use crate::network_device::tunnel_device;

#[cfg(feature = "uptime")]
use crate::uptime::uptime;

#[cfg(feature = "watson")]
use crate::watson::{watson_task, watson_time};

use crate::utils::parse_time_to_seconds;

/// A condition that has to be met to allow alerting
#[derive(Deserialize, Debug, PartialEq)]
#[serde(tag = "name")]
#[serde(rename_all = "lowercase")]
pub enum Condition {
    #[cfg(feature = "uptime")]
    Uptime {
        max_uptime: String,
        max_minutes_between_sessions: Option<i64>,
    },
    #[cfg(feature = "network")]
    #[allow(non_camel_case_types)]
    Ip_in_range { value: String },
    #[cfg(feature = "network")]
    #[allow(non_camel_case_types)]
    Ip_starts_with { value: String },
    #[cfg(feature = "network")]
    #[allow(non_camel_case_types)]
    Ip_ends_with { value: String },
    #[cfg(feature = "network")]
    #[allow(non_camel_case_types)]
    Tunnel_device { value: String },
    #[cfg(feature = "watson")]
    #[allow(non_camel_case_types)]
    Watson_task {
        tag: Option<String>,
        project: Option<String>,
    },
    #[cfg(feature = "watson")]
    #[allow(non_camel_case_types)]
    Watson_time {
        max_time: String,
        max_minutes_between_tasks: Option<i64>,
    },
}
impl Condition {
    // TODO: Result
    pub fn matches(&self) -> bool {
        match self {
            #[cfg(feature = "uptime")]
            Condition::Uptime {
                max_uptime,
                max_minutes_between_sessions,
            } => {
                let up: i64 = match uptime(*max_minutes_between_sessions) {
                    Ok(u) => u,
                    Err(_) => return false, // TODO
                };
                let max_uptime = match parse_time_to_seconds(max_uptime.to_string()) {
                    Ok(mu) => mu,
                    Err(e) => {
                        eprintln!("Error parsing max uptime: {}", e);
                        return false;
                    }
                };
                if up > max_uptime {
                    eprintln!(
                        "Uptime condition matched: up {} seconds, max_uptime: {} seconds",
                        up, max_uptime
                    );
                    true
                } else {
                    eprintln!(
                        "Uptime condition did not match: up {} seconds, max_uptime: {} seconds",
                        up, max_uptime
                    );
                    false
                }
            }
            #[cfg(feature = "network")]
            Condition::Ip_in_range { value } => match ip_in_range(value.to_string()) {
                Some(matching_ip) => {
                    eprintln!(
                        "IP_in_range condition matched: {} in {}",
                        matching_ip, value
                    );
                    true
                }
                None => {
                    eprintln!(
                        "IP_in_range condition did not match: no IP in {} found",
                        value
                    );
                    false
                }
            },
            #[cfg(feature = "network")]
            Condition::Ip_starts_with { value } => match ip_starts_with(&value) {
                Some(matching_ip) => {
                    eprintln!(
                        "IP_starts_with condition matched: {} starts with {}",
                        matching_ip, value
                    );
                    true
                }
                None => {
                    eprintln!(
                        "IP_starts_with condition did not match: no IP starting with {} found",
                        value
                    );
                    false
                }
            },
            #[cfg(feature = "network")]
            Condition::Ip_ends_with { value } => match ip_ends_with(&value) {
                Some(matching_ip) => {
                    eprintln!(
                        "IP_ends_with condition matched: {} ends with {}",
                        matching_ip, value
                    );
                    true
                }
                None => {
                    eprintln!(
                        "IP_ends_with condition did not match: no IP ending with {} found",
                        value
                    );
                    false
                }
            },
            #[cfg(feature = "network")]
            Condition::Tunnel_device { value } => match tunnel_device(&value) {
                Some(dev) => {
                    eprintln!(
                        "Tunnel_device condition matched: device {} found as match for {}",
                        dev, value
                    );
                    true
                }
                None => {
                    eprintln!(
                        "Tunnel_device condition did not match: device {} not found",
                        value
                    );
                    false
                }
            },
            #[cfg(feature = "watson")]
            Condition::Watson_task { tag, project } => match watson_task(&project, &tag) {
                Ok(s) => match s {
                    Some(task) => {
                        eprintln!("watson_task condition matched: task {} found as match for task {:?} {:?}",
                        task, project, tag);
                        true
                    }
                    None => {
                        eprintln!(
                            "watson_task condition did not match: no task {:?} {:?}",
                            project, tag
                        );
                        false
                    }
                },
                Err(e) => {
                    eprintln!("Error checking watson status: {}", e);
                    false
                }
            },
            #[cfg(feature = "watson")]
            Condition::Watson_time {
                max_time,
                max_minutes_between_tasks,
            } => {
                let task_time: i64 = match watson_time(*max_minutes_between_tasks) {
                    Ok(task_time) => task_time,
                    Err(_) => return false,
                };
                let max_time = match parse_time_to_seconds(max_time.to_string()) {
                    Ok(max_time) => max_time,
                    Err(e) => {
                        eprintln!("Error parsing max watson time: {}", e);
                        return false;
                    }
                };
                if task_time > max_time {
                    eprintln!(
                        "Watson time condition matched: logged {} seconds, maximum: {} seconds",
                        task_time, max_time
                    );
                    true
                } else {
                    eprintln!(
                        "Watson time condition did not match: logged {} seconds, maximum: {} seconds",
                        task_time, max_time
                    );
                    false
                }
            }
        }
    }
}

#[cfg(all(test, feature = "uptime"))]
mod test_parse_uptime_conditions {
    use super::*;

    #[test]
    fn should_parse_uptime() {
        let yaml = r#"---
- name: uptime
  max_uptime: 480m
  max_minutes_between_sessions: 15"#;
        let conditions: Vec<Condition> = serde_yaml::from_str(yaml).unwrap();

        assert_eq!(
            conditions[0],
            Condition::Uptime {
                max_uptime: String::from("480m"),
                max_minutes_between_sessions: Some(15)
            }
        );
    }

    #[test]
    fn should_parse_uptime_with_omitted_time_between_sessions() {
        let yaml = r#"---
- name: uptime
  max_uptime: 9h"#;
        let conditions: Vec<Condition> = serde_yaml::from_str(yaml).unwrap();

        assert_eq!(
            conditions[0],
            Condition::Uptime {
                max_uptime: String::from("9h"),
                max_minutes_between_sessions: None
            }
        );
    }
}

#[cfg(all(test, feature = "network"))]
mod test_parse_network_conditions {
    use super::*;

    #[test]
    fn should_parse_ip_in_range() {
        let yaml = r#"---
- name: ip_in_range
  value: 10.66.0.0/24"#;
        let conditions: Vec<Condition> = serde_yaml::from_str(yaml).unwrap();

        assert_eq!(
            conditions[0],
            Condition::Ip_in_range {
                value: "10.66.0.0/24".to_string()
            }
        );
    }

    #[test]
    fn should_parse_tunnel_device() {
        let yaml = r#"---
- name: tunnel_device
  value: tun0"#;
        let conditions: Vec<Condition> = serde_yaml::from_str(yaml).unwrap();

        assert_eq!(
            conditions[0],
            Condition::Tunnel_device {
                value: "tun0".to_string()
            }
        );
    }
}

#[cfg(all(test, feature = "watson"))]
mod test_parse_watson_conditions {
    use super::*;

    #[test]
    fn should_parse_watson_task_with_project() {
        let yaml = r#"---
- name: watson_task
  project: foo"#;
        let conditions: Vec<Condition> = serde_yaml::from_str(yaml).unwrap();

        assert_eq!(
            conditions[0],
            Condition::Watson_task {
                project: Some("foo".to_string()),
                tag: None,
            }
        );
    }

    #[test]
    fn should_parse_watson_task_with_tag() {
        let yaml = r#"---
- name: watson_task
  tag: foo"#;
        let conditions: Vec<Condition> = serde_yaml::from_str(yaml).unwrap();

        assert_eq!(
            conditions[0],
            Condition::Watson_task {
                tag: Some("foo".to_string()),
                project: None,
            }
        );
    }

    #[test]
    fn should_parse_watson_time() {
        let yaml = r#"---
- name: watson_time
  max_time: 8h"#;
        let conditions: Vec<Condition> = serde_yaml::from_str(yaml).unwrap();

        assert_eq!(
            conditions[0],
            Condition::Watson_time {
                max_time: "8h".to_string(),
                max_minutes_between_tasks: None
            }
        );
    }
}
