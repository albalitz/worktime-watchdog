#[cfg(feature = "notifications")]
use std::default::Default;

use std::error::Error;
use std::fmt;
use std::fs;
use std::io::Error as IOError;

#[cfg(feature = "notifications")]
use clap::crate_name;

use serde::Deserialize;

use crate::conditions::Condition;

#[derive(Deserialize, Debug)]
pub struct Config {
    #[serde(default = "Config::default_conditions")]
    conditions: Vec<Condition>,
    #[cfg(feature = "notifications")]
    #[serde(default = "NotificationConfig::default")]
    notifications: NotificationConfig,
}

impl Config {
    pub fn from_file(filename: &str) -> Result<Self, ConfigError> {
        let file_content: String = fs::read_to_string(filename)?;
        Config::from_str(&file_content)
    }

    fn from_str(config_file_content: &str) -> Result<Self, ConfigError> {
        match serde_yaml::from_str(config_file_content) {
            Ok(config) => Ok(config),
            Err(e) => Err(ConfigError::InvalidYaml(e.to_string())),
        }
    }

    pub fn conditions(&self) -> &Vec<Condition> {
        &self.conditions
    }

    #[cfg(feature = "notifications")]
    pub fn notifications(&self) -> &NotificationConfig {
        &self.notifications
    }

    fn default_conditions() -> Vec<Condition> {
        vec![Condition::Uptime {
            max_uptime: String::from("540m"),
            max_minutes_between_sessions: None,
        }]
    }
}

#[cfg(feature = "notifications")]
#[derive(Deserialize, Debug)]
pub struct NotificationConfig {
    title: String,
    body: String,
}

#[cfg(feature = "notifications")]
impl NotificationConfig {
    pub fn title(&self) -> &str {
        &self.title
    }

    pub fn body(&self) -> &str {
        &self.body
    }

    fn default_title() -> String {
        String::from(crate_name!())
    }

    fn default_body() -> String {
        String::from("Conditions matched! Stop working!")
    }
}

#[cfg(feature = "notifications")]
impl Default for NotificationConfig {
    fn default() -> Self {
        Self {
            title: Self::default_title(),
            body: Self::default_body(),
        }
    }
}

#[derive(Debug)]
pub enum ConfigError {
    FileError(String),
    InvalidYaml(String),
}
impl fmt::Display for ConfigError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            ConfigError::FileError(m) => m,
            ConfigError::InvalidYaml(m) => m,
        };
        write!(f, "{}", msg)
    }
}
impl Error for ConfigError {
    fn description(&self) -> &str {
        match self {
            ConfigError::FileError(fe) => &fe,
            ConfigError::InvalidYaml(ye) => &ye,
        }
    }
}
impl From<IOError> for ConfigError {
    fn from(e: IOError) -> Self {
        ConfigError::FileError(e.to_string())
    }
}

#[cfg(test)]
mod test_read_config {
    use std::fs::{remove_file, write};

    use super::*;

    #[test]
    fn should_read_complete_config_to_struct() {
        let filename = "/tmp/worktime-watchdog-test-should_read_complete_config_to_struct";
        let config_content = r#"---
conditions:
  - name: uptime
    max_uptime: 9h
    max_minutes_between_sessions: 10"#;
        create_tmp_config(filename, config_content);

        let cfg = Config::from_file(filename).unwrap();
        assert_eq!(
            cfg.conditions,
            vec![Condition::Uptime {
                max_uptime: String::from("9h"),
                max_minutes_between_sessions: Some(10)
            }]
        );

        cleanup_tmp_file(filename);
    }

    #[test]
    fn should_fall_back_to_defaults_for_optional_configs() {
        let filename =
            "/tmp/worktime-watchdog-test-should_fall_back_to_defaults_for_optional_configs";
        let config_content = r#"---
something: foo"#;
        create_tmp_config(filename, config_content);

        let cfg = Config::from_file(filename).unwrap();
        assert_eq!(
            cfg.conditions,
            vec![Condition::Uptime {
                max_uptime: String::from("540m"),
                max_minutes_between_sessions: None
            }]
        );

        cleanup_tmp_file(filename);
    }

    #[test]
    fn should_return_appropriate_error_for_nonexisting_file() {
        let cfg = Config::from_file("/this/should/not/exist");

        assert!(cfg.is_err());
        match cfg.err() {
            Some(err) => {
                match err {
                    ConfigError::FileError(_) => {
                        // this is the expected result. Nothing to do here
                    }
                    _ => panic!(
                        "Result should be a ConfigError::FileError. Found: {:?}",
                        err
                    ),
                }
            }
            _ => {}
        };
    }

    #[test]
    fn should_return_appropriate_error_for_invalid_condition() {
        let filename =
            "/tmp/worktime-watchdog-test-should_return_appropriate_error_for_invalid_condition";
        let config_content = r#"---
conditions:
  - name: foo
    value: bar"#;
        create_tmp_config(filename, config_content);

        let cfg = Config::from_file(filename);
        assert!(cfg.is_err());
        match cfg.err() {
            Some(err) => {
                match err {
                    ConfigError::InvalidYaml(_) => {
                        // this is the expected result. Nothing to do here
                    }
                    _ => panic!(
                        "Result should be a ConfigError::YamlError. Found: {:?}",
                        err
                    ),
                }
            }
            _ => {}
        };

        cleanup_tmp_file(filename);
    }

    // helper functions
    fn create_tmp_config(filename: &str, content: &str) {
        write(filename, content).expect(format!("Error creating temp file: {}", filename).as_str());
    }

    fn cleanup_tmp_file(filename: &str) {
        remove_file(filename).expect(format!("Error removing file: {}", filename).as_str());
    }
}
