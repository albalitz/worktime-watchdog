use pnet::datalink;

pub fn tunnel_device(device_name: &str) -> Option<String> {
    let interfaces = datalink::interfaces();
    let interface = interfaces
        .into_iter()
        .filter(|i| i.name == device_name)
        .next()?;
    Some(interface.name)
}
