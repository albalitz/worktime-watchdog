use std::cmp::Ordering;
use std::error::Error;
use std::fmt;
use std::fs;
use std::io::Error as IOError;

use time::{now, Duration, Tm};
use uucore::utmpx::*;

// https://stackoverflow.com/questions/25410028/how-to-read-a-struct-from-a-file-in-rust/54318228#54318228
#[cfg(not(target_vendor = "apple"))]
static VAR_LOG_WTMP: &'static str = "/var/log/wtmp";
#[cfg(target_vendor = "apple")]
static VAR_LOG_WTMP: &'static str = "/var/run/utmpx";
static UPTIME_FILE: &'static str = "/proc/uptime";
// the default time between sessions to still consider it a "reboot"
// this should be short enough to be unlikely to "survice" going home and restarting there
static MAX_SECONDS_BETWEEN_SESSIONS: i64 = 60 * 10;

pub fn uptime(max_seconds_between_sessions: Option<i64>) -> Result<i64, UptimeError> {
    let max_seconds_between_sessions = match max_seconds_between_sessions {
        Some(t) => t * 60, // expect the config to be minutes
        None => MAX_SECONDS_BETWEEN_SESSIONS,
    };

    let wtmp_records: Vec<Session> = read_wtmp();
    if !wtmp_records.is_empty() {
        let mut total_uptime: i64 = 0;
        let mut previous_session: Option<Session> = None;

        for session in wtmp_records.into_iter().rev() {
            match previous_session {
                Some(prev) => {
                    let distance = session.distance(&prev);
                    if distance < Duration::seconds(max_seconds_between_sessions) {
                        total_uptime += session.duration().num_seconds();
                    } else {
                        break;
                    }
                }
                None => {
                    total_uptime += session.duration().num_seconds();
                }
            }
            previous_session = Some(session);
        }
        Ok(total_uptime)
    } else {
        if cfg!(target_vendor = "apple") {
            unimplemented!()
        } else {
            eprintln!("Error reading WTMP log. Falling back to plain uptime.");
            read_uptime_file()
        }
    }
}

fn read_uptime_file() -> Result<i64, UptimeError> {
    let file_content = fs::read_to_string(UPTIME_FILE)?;
    match file_content.split_whitespace().nth(0) {
        Some(sec_float) => match sec_float.split('.').nth(0) {
            Some(sec_int) => match sec_int.parse::<i64>() {
                Ok(up) => Ok(up),
                Err(e) => return Err(UptimeError::ParseSeconds(e.to_string())),
            },
            None => Err(UptimeError::SplitDecimal(String::from(sec_float))),
        },
        None => Err(UptimeError::SplitUptimeSeconds(file_content)),
    }
}
#[derive(Debug)]
pub enum UptimeError {
    FileError(String),
    SplitUptimeSeconds(String),
    SplitDecimal(String),
    ParseSeconds(String),
}
impl fmt::Display for UptimeError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let msg = match self {
            UptimeError::FileError(m) => m,
            UptimeError::SplitUptimeSeconds(m) => m,
            UptimeError::SplitDecimal(m) => m,
            UptimeError::ParseSeconds(m) => m,
        };
        write!(f, "{}", msg)
    }
}
impl Error for UptimeError {
    fn description(&self) -> &str {
        match self {
            UptimeError::FileError(e) => &e,
            UptimeError::SplitUptimeSeconds(e) => &e,
            UptimeError::SplitDecimal(e) => &e,
            UptimeError::ParseSeconds(e) => &e,
        }
    }
}
impl From<IOError> for UptimeError {
    fn from(e: IOError) -> Self {
        UptimeError::FileError(e.to_string())
    }
}

pub fn read_wtmp() -> Vec<Session> {
    //! This effectively collects uptime data similar to `last reboot --since today`,
    //! just wrapping around and filtering uucore::utmpx::UtmpxIter
    let mut sessions: Vec<Session> = Vec::new();
    let mut session: Session = Session::new(now());

    let records = Utmpx::iter_all_records()
        .read_from(VAR_LOG_WTMP)
        .filter(|r| r.record_type() == BOOT_TIME || r.record_type() == RUN_LVL);
    for record in records {
        match record.record_type() {
            BOOT_TIME => session = Session::new(record.login_time()),
            RUN_LVL => {
                session.set_shutdown(record.login_time());
            }
            _ => (),
        }
        sessions.push(session);
    }

    sessions
}

#[derive(Copy, Clone, Debug)]
pub struct Session {
    pub boot_time: Tm,
    pub shutdown_time: Option<Tm>,
}
impl Session {
    fn new(boot_time: Tm) -> Self {
        Session {
            boot_time: boot_time,
            shutdown_time: None,
        }
    }

    fn set_shutdown(&mut self, shutdown_time: Tm) {
        self.shutdown_time = Some(shutdown_time);
    }

    pub fn duration(&self) -> Duration {
        //! Calculate the duration
        let shutdown_time = self.shutdown_time.unwrap_or(now());
        Duration::seconds(shutdown_time.to_timespec().sec - self.boot_time.to_timespec().sec)
    }

    pub fn distance(&self, other: &Self) -> Duration {
        //! Calculate the time between this session and other
        //! Assuming self happens before other, it considers self's end and other's start;
        //! assuming self happens after other, it considers other's end and self's start.
        // TODO: refactor this
        if self < other {
            match self.shutdown_time {
                Some(shutdown_time) => Duration::seconds(
                    other.boot_time.to_timespec().sec - shutdown_time.to_timespec().sec,
                ),
                None => {
                    Duration::seconds(now().to_timespec().sec - other.boot_time.to_timespec().sec)
                }
            }
        } else {
            match other.shutdown_time {
                Some(shutdown_time) => Duration::seconds(
                    self.boot_time.to_timespec().sec - shutdown_time.to_timespec().sec,
                ),
                None => Duration::seconds(
                    now().to_timespec().sec - self.shutdown_time.unwrap_or(now()).to_timespec().sec,
                ),
            }
        }
    }
}
impl PartialEq for Session {
    fn eq(&self, other: &Self) -> bool {
        self.boot_time == other.boot_time && self.shutdown_time == other.shutdown_time
    }
}
impl PartialOrd for Session {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self.shutdown_time {
            Some(sd_t) => match sd_t < other.boot_time {
                true => Some(Ordering::Less),
                false => Some(Ordering::Greater),
            },
            None => match other.shutdown_time {
                Some(sd_t) => match self.boot_time > sd_t {
                    true => Some(Ordering::Greater),
                    false => Some(Ordering::Less),
                },
                None => Some(Ordering::Greater),
            },
        }
    }
}

impl fmt::Display for Session {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let shutdown_time = match self.shutdown_time {
            Some(st) => st.asctime().to_string(),
            None => "still running".to_string(),
        };
        write!(
            f,
            "Started at {}, ended at {} - runtime: {} seconds",
            self.boot_time.asctime(),
            shutdown_time,
            self.duration().num_seconds()
        )
    }
}

#[cfg(all(test, not(target_vendor = "apple")))]
mod test_uptime {
    use super::*;

    #[test]
    fn should_find_some_kind_of_uptime() {
        assert!(uptime(None).unwrap() != 0);
    }

    #[test]
    fn should_read_uptime_file() {
        assert!(read_uptime_file().unwrap() != 0);
    }
}

#[cfg(test)]
mod test_read_wtmp {
    use super::*;

    use std::path::Path;

    fn inside_docker() -> bool {
        //! util function for checking if we're running in a Docker container - see below
        Path::new("/.dockerenv").exists()
    }

    #[test]
    fn should_have_current_session_without_shutdown_time() {
        //! this test may fail when running in a CI
        let sessions = read_wtmp();

        if !inside_docker() {
            assert!(sessions.last().unwrap().shutdown_time.is_none());
        }
    }
}

#[cfg(test)]
mod test_session_partialord {
    use super::*;

    use time::{at, Timespec};

    fn test_data_sequential_sessions() -> (Session, Session) {
        let boot_time_first = 1573336531;
        let shutdown_time_first = boot_time_first + 500;
        let diff_between_sessions = 200;
        let boot_time_second = shutdown_time_first + diff_between_sessions;
        let shutdown_time_second = boot_time_second + 500;

        let boot_time = at(Timespec::new(boot_time_first, 0));
        let shutdown_time = Some(at(Timespec::new(shutdown_time_first, 0)));
        let first = Session {
            boot_time,
            shutdown_time,
        };
        let boot_time = at(Timespec::new(boot_time_second, 0));
        let shutdown_time = Some(at(Timespec::new(shutdown_time_second, 0)));
        let second = Session {
            boot_time,
            shutdown_time,
        };

        (first, second)
    }

    #[test]
    fn should_recognize_earlier_session_as_smaller() {
        let (first, second) = test_data_sequential_sessions();
        assert!(first < second);
    }

    #[test]
    fn should_recognize_later_session_as_greater() {
        let (first, second) = test_data_sequential_sessions();
        assert!(second > first);
    }

    #[test]
    fn should_recognize_later_session_without_shutdown_time_to_be_greater() {
        let boot_time_first = 1573336531;
        let shutdown_time_first = boot_time_first + 500;
        let boot_time_second = shutdown_time_first + 200;

        let boot_time = at(Timespec::new(boot_time_first, 0));
        let shutdown_time = Some(at(Timespec::new(shutdown_time_first, 0)));
        let first = Session {
            boot_time,
            shutdown_time,
        };
        let boot_time = at(Timespec::new(boot_time_second, 0));
        let second = Session::new(boot_time);

        assert!(second > first);
    }

    #[test]
    fn should_return_none_when_first_session_is_missing_shutdown_time() {
        let boot_time_first = 1573336531;
        let boot_time_second = boot_time_first + 200;

        let boot_time = at(Timespec::new(boot_time_first, 0));
        let first = Session::new(boot_time);
        let boot_time = at(Timespec::new(boot_time_second, 0));
        let shutdown_time = Some(at(Timespec::new(boot_time_second, 0)));
        let second = Session {
            boot_time,
            shutdown_time,
        };

        assert!(second > first);
    }
}

#[cfg(test)]
mod test_session_set_shutdown {
    use super::*;

    #[test]
    fn should_update_shutdown_time() {
        let shutdown_time = now();
        let mut session = Session::new(now());

        assert!(session.shutdown_time.is_none());

        session.set_shutdown(shutdown_time);
        assert_eq!(session.shutdown_time, Some(shutdown_time));
    }
}

#[cfg(test)]
mod test_session_duration {
    use super::*;
    use time::{at, Timespec};

    #[test]
    fn should_calculate_correct_duration() {
        let timestamp = 1573336531;
        let difference = 500;

        let boot_time = at(Timespec::new(timestamp, 0));
        let shutdown_time = Some(at(Timespec::new(timestamp + difference, 0)));
        let session = Session {
            boot_time,
            shutdown_time,
        };

        assert_eq!(session.duration(), Duration::seconds(difference));
    }

    #[test]
    fn should_not_break_for_negative_durations() {
        //! This may happen when the clock is wrong at boot time and corrected when there's a
        //! connection to an NTP server
        let timestamp = 1573336531;
        let difference = -500;

        let boot_time = at(Timespec::new(timestamp, 0));
        let shutdown_time = Some(at(Timespec::new(timestamp + difference, 0)));
        let session = Session {
            boot_time,
            shutdown_time,
        };

        assert_eq!(session.duration(), Duration::seconds(difference));
    }
}

#[cfg(test)]
mod test_session_distance {
    use super::*;

    use time::{at, Timespec};

    #[test]
    fn should_return_correct_distance_to_future_session() {
        let boot_time_first = 1573336531;
        let shutdown_time_first = boot_time_first + 500;
        let diff_between_sessions = 200;
        let boot_time_second = shutdown_time_first + diff_between_sessions;
        let shutdown_time_second = boot_time_second + 500;

        let boot_time = at(Timespec::new(boot_time_first, 0));
        let shutdown_time = Some(at(Timespec::new(shutdown_time_first, 0)));
        let first = Session {
            boot_time,
            shutdown_time,
        };
        let boot_time = at(Timespec::new(boot_time_second, 0));
        let shutdown_time = Some(at(Timespec::new(shutdown_time_second, 0)));
        let second = Session {
            boot_time,
            shutdown_time,
        };

        assert_eq!(
            first.distance(&second),
            Duration::seconds(diff_between_sessions)
        );
    }

    #[test]
    fn should_return_correct_distance_to_past_session() {
        let boot_time_first = 1573336531;
        let shutdown_time_first = boot_time_first + 500;
        let diff_between_sessions = 200;
        let duration_second = 400;
        let boot_time_second = boot_time_first - duration_second - diff_between_sessions;
        let shutdown_time_second = boot_time_second + duration_second;

        let boot_time = at(Timespec::new(boot_time_first, 0));
        let shutdown_time = Some(at(Timespec::new(shutdown_time_first, 0)));
        let first = Session {
            boot_time,
            shutdown_time,
        };
        let boot_time = at(Timespec::new(boot_time_second, 0));
        let shutdown_time = Some(at(Timespec::new(shutdown_time_second, 0)));
        let second = Session {
            boot_time,
            shutdown_time,
        };

        assert_eq!(
            first.distance(&second),
            Duration::seconds(diff_between_sessions)
        );
    }
}
